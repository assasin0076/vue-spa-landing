# Тут можно потыкать приложение

https://vue-spa-landing.vercel.app

# Текст ТЗ

Правый блок с перечнем линеек - фильтр по бренду (в Json data.propsInfo.brandTree);
В списке продуктовых линеек могут быть родительские и дочерние элементы. По клику
на родительский элемент открывается список дочерних.
По умолчанию первый элемент списка активный. Если первый элемент списка
родительский, то активным будет его первый дочерний элемент.

Левый блок с характеристиками продукции - фильтрация внутри продуктовой линейки -
объем, газированность, тип упаковки. Если у всех товаров данного бренда значение
одной из характеристик одинаковое, отображать значение характеристики в блоке
фильтрации без выпадающего списка. Значения фильтров взаимосвязаны - если
выбрано значение “Газированная”, то в списке объемов не должен отображаться
фильтр “19л”, т.к. газированной воды в 19 литровой бутыли в списке товаров не будет.
(свойства volumeList, gassingList, typeList, capList в data.propsInfo)

Заголовок и текст в левом блоке зависит от выбранного бренда в правой части
(data.propsInfo.brandInfo).

Средний блок - отфильтрованное предложение товара. (data.products) Изображение
товара получаем из элемента товара в каталоге (data.products.{i}.picture). За каждым
брендом закреплено свое фоновое изображение (выводим его за бутылкой)
(data.propsInfo.brandInfo), таким образом изображения с буквой “А” будет одинаковым
для всех товаров бренда “Sport std”.

Средний блок содержит изображение с помещенными на нем точками. Для активной
точки отображается модальное окно с изображением, заголовком и описанием. Первая
точка активна по умолчанию. Переключение модальных окон происходит по клику на
точку. (data.products.{i}.points)

# spa-landing

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
