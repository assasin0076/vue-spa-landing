export default {
  volumeList: "volume",
  gassingList: "gassing",
  typeList: "type",
  capList: "cap",
  ru: {
    volume: "Объем",
    gassing: "Газировка",
    type: "Тип",
    cap: "Крышка",
  },
};
