import data from "../data/data.json";
import translateFilters from "../dictionaries/translateFilters";

export const getProduct = (brandId) => {
  return data.data.products.find((product) => product.brand === brandId);
};

export const getBrandInfo = (brandId) => {
  return { ...data.data.propsInfo.brandInfo[brandId], id: brandId };
};

export const getBrandsList = () => {
  const tree = data.data.propsInfo.brandTree;
  const list = [];
  Object.keys(tree).forEach((key) => {
    const branch = tree[key];
    if (!branch.children) list.push(branch);
    else branch.children.forEach((leaf) => list.push(leaf));
  });
  return list;
};

export const getVolumeList = () => {
  return data.data.propsInfo.volumeList;
};

export const getAllFilters = () => {
  const accessedKeys = ["volumeList", "gassingList", "typeList", "capList"];
  const filters = {};
  accessedKeys.forEach((key) => {
    filters[key] = data.data.propsInfo[key];
  });
  return filters;
};

export const getFilters = () => {
  const filters = getAllFilters();
  return Object.keys(filters).map((key) => {
    const filter = filters[key];
    const content = Object.keys(filter).map((elementKey) => ({
      ...filter[elementKey],
    }));
    return { key: translateFilters[key], content };
  });
};

export const getProducts = (filters) => {
  const products = data.data.products;
  if (!filters) return products;
  return products.filter((product) => {
    return !filters.some((filter) => {
      return filter.value !== product[filter.name];
    });
  });
};
