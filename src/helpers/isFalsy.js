const arrayHandler = (array) => {
  return !array.length;
};

const objectHandler = (object) => {
  return !Object.keys(object).length;
};

/**
 * Проверяет пуст ли массив/объект, false ли другие значения
 * @param {any} value - Значение любого типа
 */
export default (value) => {
  const handlers = {
    Array: arrayHandler,
    Object: objectHandler,
  };
  const exceptionTypes = Object.keys(handlers);
  const type = value.constructor.name;
  if (!exceptionTypes.includes(type)) return !value;
  return handlers[type](value);
};
