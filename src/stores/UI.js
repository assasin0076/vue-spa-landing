import { defineStore } from "pinia";
import { getBrandInfo } from "../services/fakeApi";
import emitter from "../services/emitter";

export const useUIStore = defineStore({
  id: "UI",
  state: () => ({
    selectedBrand: null,
    filters: [],
    availableProducts: [],
  }),
  actions: {
    resetFilters() {
      this.filters = [];
    },
    selectBrand(brand) {
      this.selectedBrand = null;
      this.resetFilters();
      this.addFilter({ name: "brand", value: brand.id });
      this.selectedBrand = getBrandInfo(brand.id);
    },
    addFilter(filter) {
      const findSelectedFilter = () => {
        return this.filters.find((storeFilter) => {
          return storeFilter.name === filter.name;
        });
      };
      const selectedFilter = findSelectedFilter();

      const pushFilter = () => {
        this.filters.push(filter);
        emitter.emit("filters_updated");
      };
      const changeFilter = () => {
        const isFilterDidNotChange = selectedFilter.value === filter.value;
        if (isFilterDidNotChange) return;
        selectedFilter.value = filter.value;
        emitter.emit("filters_updated");
      };

      if (!selectedFilter) pushFilter(filter);
      else changeFilter();
    },
    setAvailableProducts(products) {
      this.availableProducts = products;
    },
  },
});
